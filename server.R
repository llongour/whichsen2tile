server <- function(input, output, session) {
  observe({
    query <- parseQueryString(session$clientData$url_search)
    tile_param <- if (!is.null(query[['tile']])) query[['tile']] else ""
    
    tile <- unlist(strsplit(tile_param, ','))
    
    output$map <- renderLeaflet({
      map <- leaflet() %>%
        addTiles(group = "OSM (default)") %>%
        addProviderTiles(providers$Esri.WorldImagery, group = "Esri World Imagery") %>%
        addMiniMap() %>%
        addLayersControl(
          baseGroups = c("OSM (default)", "Esri World Imagery")
        )
      
      if (length(tile) > 0) {
        map <- map %>% addPolygons(
          data = open_dataset("sentinel2_grid.parquet") %>%
            filter(Name %in% tile) %>%
            read_sf_dataset(),
          label = ~Name,
          fillOpacity = 0,
          color = "red"
        )
      }
      
      map
    })
  })
}
